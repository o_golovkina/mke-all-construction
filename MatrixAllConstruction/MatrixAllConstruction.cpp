﻿#include <iostream>
#include <clocale>
#include <vector>
#include <deque>
#include <string>
#include <math.h>
#include <iterator>
#include <algorithm>
#include <iomanip>
using namespace std;
int n = 0;
double E = 1, F = 1, J = 1;

class Sterjhen {
	int id_st, id_p_1, id_p_2, x1, y1, x2, y2;
	double l, sin, cos;
public:
	Sterjhen() :
		id_st(0),
		id_p_1(0),
		id_p_2(0),
		x1(0),
		y1(0),
		x2(0),
		y2(0),
		l(0),
		sin(0),
		cos(0) {}
	Sterjhen(
		int new_id_st,
		int new_id_p1,
		int new_id_p2,
		int new_x1,
		int new_y1,
		int new_x2,
		int new_y2
	) :
		id_st(new_id_st),
		id_p_1(new_id_p1),
		id_p_2(new_id_p2),
		x1(new_x1),
		y1(new_y1),
		x2(new_x2),
		y2(new_y2),
		l(sqrt((new_x2 - new_x1) ^ 2 + (new_y2 - new_y1) ^ 2)),
		sin((new_y2 - new_y1 / l)),
		cos((new_x2 - new_x1 / l)) 
	{

	}

	~Sterjhen() {}


	int getIdSt() const { return id_st; }
	int getP1() const { return id_p_1; }
	int getP2() const { return id_p_2; }
	int getX1() const { return x1; }
	int getY1() const { return y1; }
	int getX2() const { return x2; }
	int getY2() const { return y2; }
	double getL() const { return l; }
	
	double getSin() const { return sin; }
	double getCos() const { return cos; }
};
class Uzel {
	int id_p, x, y;
public:
	Uzel(
		int new_id_p,
		int new_x,
		int new_y
	) :
		id_p(new_id_p),
		x(new_x),
		y(new_y) {}
	Uzel() :
		id_p(0),
		x(0),
		y(0) {}
	int getId() const { return id_p; }
	int getX() const { return x; }
	int getY() const { return y; }
};
class MX {
	int id_st;
	Sterjhen* st;
	vector<vector<double>> RSL_loc;
	vector<vector<double>> RSL_glob;
	vector<vector<double>> V;

	vector<vector<double>> zerosmx(size_t size) {
		vector<vector<double>> mx(size, vector<double>(size, 0));
		for (size_t i = 0; i < size; i++) {
			mx[i].resize(size);
			for (size_t j = 0; j < size; j++)
				mx[i][j] = 0;
		}
		return mx;
	}
	vector<vector<double>> enterRSLloc(vector<vector<double>> rsl,
		double E, double S, double In, double l) {

		rsl[0][0] = rsl[3][3] = E * S / l;
		rsl[1][1] = rsl[4][4] = 12 * E * In / pow(l, 3);
		rsl[2][1] = rsl[1][2] = rsl[1][5] = rsl[5][1] = 6 * E * In /
			pow(l, 2);
		rsl[0][3] = rsl[3][0] = -E * S / l;
		rsl[4][1] = rsl[1][4] = -12 * E * In / pow(l, 3);
		rsl[4][2] = rsl[2][4] = rsl[5][4] = rsl[4][5] = -6 * E * In
			/ pow(l, 2);
		rsl[5][2] = rsl[2][5] = 2 * E * In / l;
		rsl[2][2] = rsl[5][5] = 4 * E * In / l;

		return rsl;
	}

	vector<vector<double>> enterV(vector<vector<double>> v, double
		sin, double cos) {

		v[0][0] = v[1][1] = v[4][4] = v[3][3] = cos;
		v[0][1] = v[3][4] = sin;
		v[1][0] = v[4][3] = -sin;
		v[2][2] = v[5][5] = 1;

		return v;
	}
public:
	MX() {}
	MX(
		Sterjhen* new_st
	) :
		st(new_st),
		id_st(new_st->getIdSt()) {
		RSL_loc = zerosmx(3 * 2);
		V = zerosmx(3 * 2);
		RSL_glob = zerosmx(3 * 2);
		RSL_loc = enterRSLloc(RSL_loc, E, F, J, new_st->getL());
		V = enterV(V, new_st->getSin(), new_st->getCos());
		RSL_glob = multiply66(multiply66(transp(V), RSL_loc), V);
		cout << "Локальная матрица жесткости" << endl;
		print(RSL_loc);
		cout << endl;
		cout << "Матрица перехода" << endl;
		print(V);
		cout << "Глобальная матрица жесткости" << endl;
		print(RSL_glob);
	}

	int getIdSt() { return id_st; }

	vector<vector<double>> getRSLLoc() const { return RSL_loc; }
	vector<vector<double>> getRSLGlob() const { return RSL_glob; }
	vector<vector<double>> getV() const { return V; }
	vector<vector<double>> transp(vector<vector<double>> mx) const {
		vector<vector<double>> newmx(mx.size());
		for (int i = 0; i < mx.size(); i++) {
			newmx[i].resize(mx.size());
			for (int j = 0; j < mx.size(); j++)
				newmx[i][j] = mx[j][i];
		}
		return newmx;
	}
	vector<vector<double>> multiply66(vector<vector<double>> mx1,
		vector<vector<double>> mx2) const {
		vector<vector<double>> newmx(mx1.size());
		for (int i = 0; i < mx1.size(); i++) {
			newmx[i].resize(mx1.size());
			for (int j = 0; j < mx1.size(); j++)
				for (int k = 0; k < mx1.size(); k++)
					newmx[i][j] += mx1[i][k] * mx2[k][j];
		}
		return newmx;
	}
	virtual void print(vector<vector<double>> mx) {
		for_each(mx.begin(), mx.end(), [](vector<double>& ivec)
			{
				for_each(ivec.begin(), ivec.end(), [](double i)
					{
						cout << left << setw(5) << i;
					});
				cout << endl;
			});
	}
};
class St_list : Sterjhen {
	deque<Sterjhen*> lst_st;
public:
	deque<Sterjhen*> getLst() const { return lst_st; }
	void add(int id_st, int id_p1, int id_p2, int x1, int y1, int x2,
		int y2) {
		lst_st.push_back(new Sterjhen(id_st, id_p1, id_p2, x1, y1,
			x2, y2));
	}
	void print() {
		cout << "================================" << endl;
		cout << " Данные о стержнях " << endl;
		cout << "================================" << endl;
		for (Sterjhen* i : lst_st) {
			cout << "Стержень №" << i->getIdSt() << endl;
			cout << "№ узла начала стержня: " << i->getP1() <<
				endl;
			cout << "№ узла конца стержня: " << i->getP2() <<
				endl;
			cout << "Его координаты: " << endl << "x1 = " << i-> getX1()
				<< ", y1 = " << i->getY1()
				<< ", x2 = " << i->getX2()
				<< ", y2 = " << i->getY2() << endl;
			cout << "Его длина: " << i->getL() << endl;
			cout << "================================" << endl;
		}
	}
	int enter_num(int uzl) {
		int num;
		string input;
		while (1) {
			cout << "Введите количество стержней" << endl;
			cout << "st = "; cin >> input;
			try {
				if (input == "0") {
					string n = "nope";
					throw n;
				}
				if (!atoi(input.c_str()))
					throw 'a';
				else if (atoi(input.c_str()) > uzl * (uzl - 1) /
					2)
					throw false;
				else if (atoi(input.c_str()) < uzl - 1) {
					string n = "nope";
					throw n;
				}
				else return atoi(input.c_str());
			}
			catch (bool no_ok) {
				cerr << "Количество стержней превышает количествостержней, которое можно построить из " << uzl << " узлов" << endl;
					getchar();
			}
			catch (char no_ok) {
				cerr << "Пожалуйста, введите целое число!" <<
					endl;
				getchar();
			}
			catch (string no_ok) {
				cerr << "Введенное количество стержней не покроет все " << uzl << " узлов" << endl;
					getchar();
			}
		}
	}
};
class Uzl_list : Uzel {
	deque<Uzel*> lst_uzl;
public:
	deque<Uzel*> getLst() { return lst_uzl; }
	void add(int id_p, int x, int y) {
		lst_uzl.push_back(new
			Uzel(id_p, x, y));
	}
	void print() {
		cout << "================================" << endl;
		cout << " Данные об узлах " << endl;
		cout << "================================" << endl;
		for (Uzel* i : lst_uzl) {
			cout << "Узел №" << i->getId() << endl;
			cout << "Его координаты: " << endl << "x = " << i -> getX()
				<< ", y = " << i->getY() << endl;
			cout << "================================" << endl;
		}
	}
	int enter_num() {
		int num;
		string input;
		while (1) {
			cout << "Введите количество уникальных узлов" << endl;
			cout << "n = "; cin >> input;
			try
			{
				if (input == "0")
					throw - 1;
				if (!atoi(input.c_str()))
					throw 'a';
				else if (atoi(input.c_str()) <= 1)
					throw - 1;
				else {
					return atoi(input.c_str());
				}
			}
			catch (char no_ok) {
				cerr << "Пожалуйста, введите целое число!" <<
					endl;
				getchar();
			}
			catch (int no_ok) {
				cerr << "Количество узлов должно быть хотя бы 2!"
					<< endl;
				getchar();
			}
		}
	}
	Uzel* find(int p) {
		for (Uzel* i : lst_uzl)
			if (i->getId() == p) {
				return i;
			}
		cout << "Такого узла не существует!" << endl;
		return nullptr;
	}
};
class MX_list : MX {
	deque<MX*> lst_mxv;
	vector<vector<double>> RG;
	vector<vector<double>> zerosmx(size_t size) {
		vector<vector<double>> mx(size, vector<double>(size, 0));
		for (size_t i = 0; i < size; i++) {
			mx[i].resize(size);
			for (size_t j = 0; j < size; j++)
				mx[i][j] = 0;
		}
		return mx;
	}
public:
	void setRG(vector<vector<double>> newRG) { RG = newRG; }
	vector<vector<double>> getRG() { return RG; }
	deque<MX*> getList() const { return lst_mxv; }
	MX_list() {
		RG = zerosmx(n * 3);
	}
	void add(Sterjhen* input) { lst_mxv.push_back(new MX(input)); }
	void print(vector<vector<double>> mx) {
		for_each(mx.begin(), mx.end(), [](vector<double>& ivec)
			{
				for_each(ivec.begin(), ivec.end(), [](double i)
					{
						cout << left << setw(5) << i;
					});
				cout << endl;
			});
	}
};
vector<vector<double>> solve(MX_list mx_lst, St_list st_list) {
	vector<vector<double>> newmx = mx_lst.getRG();
	for (int i = 0; i < st_list.getLst().size(); i++) {
		for (int j = 0; j < 3; j++)
			for (int k = 0; k < 3; k++) {
				newmx[(st_list.getLst()[i]->getP1() - 1) * 3 +
					j][(st_list.getLst()[i]->getP1() - 1) * 3 + k] += mx_lst.getList()[i] -> getRSLGlob()[j][k];
				newmx[(st_list.getLst()[i]->getP1() - 1) * 3 +
					j][(st_list.getLst()[i]->getP2() - 1) * 3 + k] += mx_lst.getList()[i] -> getRSLGlob()[j][k + 3];
				newmx[(st_list.getLst()[i]->getP2() - 1) * 3 +
					j][(st_list.getLst()[i]->getP1() - 1) * 3 + k] += mx_lst.getList()[i] -> getRSLGlob()[j + 3][k];
				newmx[(st_list.getLst()[i]->getP2() - 1) * 3 +
					j][(st_list.getLst()[i]->getP2() - 1) * 3 + k] += mx_lst.getList()[i] -> getRSLGlob()[j + 3][k + 3];
			}
	}
	return newmx;
}
int main()
{
	setlocale(LC_ALL, "RUS");

	Uzl_list uzl;
	St_list st;

	int n_st, x, y, p1, p2;

	n = uzl.enter_num();

	for (int i = 0; i < n; i++) {
		cout << "Введите координаты " << i + 1 << "-го узла" <<
			endl;
		cout << "x = "; cin >> x; cout << "y = "; cin >> y;
		uzl.add(i + 1, x, y);
	}

	n_st = st.enter_num(n);

	Uzel* p1_lst, * p2_lst;

	for (int i = 0; i < n_st; i++) {
		while (1) {
			cout << "Введите номер начального узла для " << i + 1
				<< " стержня" << endl;
			cout << "p1 = "; cin >> p1;
			p1_lst = uzl.find(p1);
			if (p1_lst)
				break;
		}
		while (1) {
			cout << "Введите номер конечного узла для " << i + 1
				<< " стержня" << endl;
			cout << "p2 = "; cin >> p2;
			if (p1 == p2) {
				cout << "№ узла начала стержня не должен совпадать № узла конца!" << endl;
					continue;
			}
			p2_lst = uzl.find(p2);
			if (p2_lst) {
				st.add(i + 1, p1_lst->getId(), p2_lst->getId(),
					p1_lst->getX(), p1_lst->getY(), p2_lst->getX(), p2_lst->getY());
				break;
			}
		}
	}

	cout << endl;
	MX_list mx;

	for (int i = 0; i < st.getLst().size(); i++)
		mx.add(st.getLst()[i]);

	mx.setRG(solve(mx, st));

	cout << "Матрица всей конструкции RG" << endl;

	mx.print(mx.getRG());
}